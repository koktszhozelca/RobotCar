//Light sensor library
#include <Wire.h>
int BH1750_address = 0x23; // i2c Address, <-- Do we need to modify this address?
byte lightSensor_buff[2];
void BH1750_Init (int address){
  Wire.beginTransmission(address);
  Wire.write(0x10); // 1 [lux] aufloesung
  Wire.endTransmission();
}
byte BH1750_Read (int address){
  byte i=0;
  Wire.beginTransmission(address);
  Wire.requestFrom(address, 2);
  while(Wire.available()){
    lightSensor_buff[i] = Wire.read(); 
    i++;
  }
  Wire.endTransmission();  
  return i;
}

//Library wrapper
void lightSensorInit(){
  Wire.begin();
  BH1750_Init(BH1750_address);
  delay(200);    
}

int getLightSensorValue(){  
  float valf = 0;
  if(BH1750_Read(BH1750_address)==2){
    valf=((lightSensor_buff[0]<<8)|lightSensor_buff[1])/1.2;
    return valf<0? -1 : (int)valf;
  }
  return -1;
}
