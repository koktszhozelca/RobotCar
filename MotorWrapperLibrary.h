//Motor library
typedef struct Motor {
  int enPin;    //(Analog)
  int inPin_1;  //(Digital)
  int inPin_2;  //(Digital)
} Motor;

void motorSetup(Motor* motor){
  pinMode(motor->enPin, OUTPUT);
  pinMode(motor->inPin_1, OUTPUT);
  pinMode(motor->inPin_2, OUTPUT);
}

void motorSetPin (Motor* motor, int enPin, int in1, int in2){
  motor->enPin = enPin;
  motor->inPin_1 = in1;
  motor->inPin_2 = in2;  
  motorSetup(motor);    
}

//HIGH --> LOW   FORWARD
//LOW --> HIGH BACKWARD
//LOW --> LOW OFF

void forward(Motor* motor, int speed){
  digitalWrite(motor->inPin_1, HIGH);
  digitalWrite(motor->inPin_2, LOW);
  analogWrite(motor->enPin, speed);
}

void backward(Motor* motor, int speed){
  digitalWrite(motor->inPin_1, LOW);
  digitalWrite(motor->inPin_2, HIGH);
  analogWrite(motor->enPin, speed);
}

void stopMotorSlow(Motor* motor){
  digitalWrite(motor->inPin_1, LOW);
  digitalWrite(motor->inPin_2, LOW);
}

void stopMotorFast(Motor* motor){
  digitalWrite(motor->inPin_1, HIGH);
  digitalWrite(motor->inPin_2, HIGH);
}


