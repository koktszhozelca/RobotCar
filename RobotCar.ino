#include "SelfLibrary.h"
#include "LightSensorWrapperLibrary.h"
#include "MotorWrapperLibrary.h"
#include "UltraSonicWrapperLibrary.h"
#include "Configuration.h"

/*
 * READ ME
 * 
 * Please the sensor one by one.
 * 
 * Serial.begin(9600);
 * Serial.print("Msg: ")
 * Serial.print(value);
 * 
 * Car setup
 * 
 * Not sure: 
 *  1. Define the pin / address light sensor: int BH1750_address = 0x23;
 *  2. The direction for the motor. HIGH then LOW is forward or backward
 * 
 * Steps:
 *  MOTOR
 *  1. Define 2 analog pins for motors (Left, Right)
 *  2. Define 4 digital pins for motors (Left, Right)
 *  3. Create Motor structure by 
 *      Motor* motorL = malloc(sizeof(Motor));
 *      motorSetPin(motorL, en, in1, in2);
 *  4. Control each motor [Forward/Backward/Stop]
 *      forward(motorL, 200);
 *      backward(motorL, 200);
 *      stopMotor(motorL);
 *      
 *  ULTRA SONIC
 *  1. Define 2 digital pins (trigPin, echoPin) *  
 *      UltraSonic* ultraSonic = malloc(sizeof(UltraSonic));  
 *      ultraSonic->trigPin = 9;
 *      ultraSonic->echoPin = 10;
 *  2. Setup sensor by 
 *      ultraSonicSetup(trigPin, echoPin);
 */
//
 UltraSonic* ultraSonic = (UltraSonic*) malloc(sizeof(UltraSonic));  
 Motor* motorL = (Motor*) malloc(sizeof(Motor));
 Motor* motorR = (Motor*) malloc(sizeof(Motor));

void setup() {
  //Setup motor
//  motorL->enPin = ??;
//  motorL->inPin_1 = ??;
//  motorL->inPin_2 = ??;
//
//  motorR->enPin = ??;
//  motorR->inPin_1 = ??;
//  motorR->inPin_2 = ??;
  
  //Setup light sensor
  lightSensorInit();

  //Setup ulrtaSonic
  ultraSonic->trigPin = 9; //Change this
  ultraSonic->echoPin = 10; //Change this
  ultraSonicSetup(ultraSonic);
  
}

void loop() {
  // put your main code here, to run repeatedly:
  int value = getLightSensorValue();
  bool isObstacleExist = hasObstacle(ultraSonic);
}
