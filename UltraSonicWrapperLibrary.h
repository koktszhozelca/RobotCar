/*
* Ultrasonic Sensor HC-SR04 and Arduino Tutorial
*
* Crated by Dejan Nedelkovski,
* www.HowToMechatronics.com
*
* Define 2 pins ( trigPin and echoPin )
*
* Safe distance = ??
*/
typedef struct UltraSonic {
  int trigPin;
  int echoPin;
} UltraSonic;

void ultraSonicSetup(UltraSonic* us){
  pinMode(us->trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(us->echoPin, INPUT); // Sets the echoPin as an Input
}

bool hasObstacle(UltraSonic* us){
  long duration;
  int distance;
  // Clears the trigPin
  digitalWrite(us->trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(us->trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(us->trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(us->echoPin, HIGH);
  // Calculating the distance
  distance= duration*0.034/2;
  return distance <= 32;
}

//void setup() {
//  ultraSonicSetup(trigPin, echoPin);
//  Serial.begin(9600); // Starts the serial communication
//}
//
//void loop() {
//  if(!hasObstacle()) return;
//  Serial.print("Has Obstacle\n");
//}
